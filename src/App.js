import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import logo from './logo.svg';
import Navbar from './components/Layout/Navbar';
import Home from './components/Pages/Home';
import { CustomersList } from './components/Pages/Customer/CustomersList';
import CustomerCreate from './components/Pages/Customer/CustomerCreate';
import Header from './components/Layout/Header';
import Footer from './components/Layout/Footer';
import NotFound from './components/NotFound/NotFound';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link, browserHistory
} from "react-router-dom";

import './App.css';

function App() {
  return (

    <div className="App">

      <Router>
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/customers" component={CustomersList} />
          <Route exact path="/create" component={CustomerCreate} />
          <Route component={NotFound} />
        </Switch>
      </Router>


    </div>
  );
}

export default App;
