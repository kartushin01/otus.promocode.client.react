import React from "react"
import logo from '../../logo.svg'

function Home() {
    return (
        <div className="row  justify-content-center">
            <div className="col-md-6">
                <img src={logo} className="App-logo" alt="logo" />
                <h2>Home</h2>

            </div>
        </div>
    );
}
export default Home;
