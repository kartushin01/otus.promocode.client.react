import React, { Component } from 'react';
import API from '../../../utils/api';
import { Link } from "react-router-dom";

export class CustomersList extends Component {

    constructor(props) {
        super(props);
        this.state = { customers: [] };
    }

    componentDidMount() {

        API.get("api/v1/Customers")
            .then(res => {
                this.setState({ customers: res.data });
                console.log(this.state.customers);
            })
    }

    render() {
        return (

            <div className="container">
                <div className="d-flex justify-content-between py-2">
                    <h1 className="h3">Customers</h1>
                    <Link to="/create" className="nav-link btn-primary">Add customer</Link>
                </div>
                <table className="table">
                    <tr>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>email</th>
                        <th></th>
                    </tr>
                    {this.state.customers.map(p => (
                        <tr>
                            <td>{p.firstName}</td>
                            <td>{p.lastName}</td>
                            <td>{p.email}</td>
                            <td><button className="btn btn-info">edit</button></td>
                        </tr>
                    ))}

                </table>
            </div >

        );
    }
}

export default CustomersList;