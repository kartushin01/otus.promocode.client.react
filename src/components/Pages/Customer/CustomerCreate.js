import React, { useState, useEffect } from 'react';
import { useForm } from "react-hook-form";
import API from '../../../utils/api';

function CustomerCreate() {

    const { register, handleSubmit, errors } = useForm(); // initialize the hook

    const [hasError, setErrors] = useState(false)

    const [isSend, setSend] = useState(false)

    const [preferences, setPreferences] = useState([]);


    async function fetchData() {
        const res = await fetch("https://localhost:5001/api/v1/Preferences");
        res
            .json()
            .then(res => setPreferences(res))
            .catch(err => setErrors(err));
    }

    useEffect(() => {
        fetchData()
    }, []);

    const onSubmit = (data) => {
        console.log(data);
        API.post("/api/v1/Customers", data).then(send => setSend(true)).catch(err => setErrors(err));

    }

    if (isSend) {
        return <p className="alert alert-secondary m-5"><em>isSend...</em></p>;
    }
    return (

        < div className="container" >

            <form onSubmit={handleSubmit(onSubmit)} className="my-5">
                <div className="form-group row">
                    <label htmlFor="firstname" className="col-sm-2 col-form-label">Имя</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="firstname"
                            className="form-control"
                            id="firstname"
                            ref={register({ required: "Введите имя", minLength: 1 })}
                        />
                        {errors.firstname && <p className="text-danger">{errors.firstname.message}</p>}
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="lastname" className="col-sm-2 col-form-label">Фамилия</label>
                    <div className="col-sm-10">
                        <input type="text"
                            name="lastname"
                            className="form-control" id="lastname"
                            ref={register({ required: "Введите Фамилию", minLength: 1 })}
                        />
                        {errors.lastname && <p className="text-danger">{errors.lastname.message}</p>}
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                    <div className="col-sm-10">
                        <input type="email"
                            name="email"
                            className="form-control"
                            id="email"
                            ref={register({
                                required: "Required",
                                pattern: {
                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                    message: "не корретный email"
                                }
                            })}
                        />
                        {errors.email && <p className="text-danger">{errors.email.message}</p>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label" for="PreferenceIds">Предпочтнеия</label>
                    <div className="col-sm-10">
                        <select multiple className="form-control" id="PreferenceIds" name="PreferenceIds" ref={register({ required: "Укажите предпочтнеие(я)" })}>

                            {preferences.map(p => (
                                <option value={p.id}>{p.name}</option>
                            ))}

                        </select>
                        {errors.PreferencesIds && <p className="text-danger">{errors.PreferencesIds.message}</p>}
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-10 col-form-label"></label>
                    <div className="col-sm-2">
                        <input type="submit" className="btn btn-primary btn-block my-4" value="Save" />
                    </div>
                </div>
            </form>
        </div >
    )
}

export default CustomerCreate
