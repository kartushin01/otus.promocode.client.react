import React from "react";

function Footer() {
    return (
        <footer className="bg-light py-5">
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <h3 className="h4">
                            Promocode Factory
                        </h3>
                    </div>
                    <div className="col-md-6">
                        <h3 className="h4">
                            Заголовок
                        </h3>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deleniti at ipsum magni laboriosam earum beatae modi. Fugit autem, dolorum, error fuga dolore, quia nisi porro sapiente architecto accusamus ullam assumenda?</p>

                    </div>
                    <div className="col-md-3">
                        <ul className="list-group">
                            <li class="list-group-item">link1</li>
                            <li class="list-group-item">link2</li>
                            <li class="list-group-item">link3</li>
                            <li class="list-group-item">link4</li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;