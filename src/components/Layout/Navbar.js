import React from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
    return (

        <div className="bg-light">
            <div className="container ">
                <nav className="navbar navbar-expand-lg navbar-light">
                    <a className="navbar-brand" href="#">Brand</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link to="/" className="nav-link">Home</Link>
                            </li>

                            <li className="nav-item">
                                <Link to="/customers" className="nav-link">Customers</Link>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
        </div>


    );
};

export default NavBar;
